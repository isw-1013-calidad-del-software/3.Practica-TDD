﻿using System;

namespace utilidades
{
    class Utilidades
    {
        public Utilidades()
        {

        }

        public static string buscar(char caracter, string cadena)
        {
            string[] words = cadena.Split(caracter);
            string result = "";
            if (words.Length <= 2)
            {
                result = "Not found!";
            }
            else if (words.Length == 3)
            {
                if (!words[1].Equals(""))
                {
                    result = "[" + words[1] + "]";
                }
                else
                {
                    result = "Not found!";
                }   
            }
            else
            {
                for (int i = 1; i < words.Length-1; i++)
                {
                    string word = words[i];
                    if (!word.Equals(""))
                    {
                        result = result + "[" + words[i] + "] ";
                    }
                }
            }    
            return result;
        }
    }
}