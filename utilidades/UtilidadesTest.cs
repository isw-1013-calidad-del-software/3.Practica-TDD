using NUnit.Framework;
using System;

namespace utilidades
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones normales
        [Test]
        public void BuscarTest1()
        {
            //Arrange
            char caracter = '-';
            string cadena = "a-bb-ccc";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("[bb]", result);
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones de ausencia de guiones 
        [Test]
        public void BuscarTest2()
        {
            //Arrange
            char caracter = '-';
            string cadena = "aaabbccc";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("Not found!", result);
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones de mas de un resultado.
        [Test]
        public void BuscarTest3()
        {
            //Arrange
            char caracter = '-';
            string cadena = "aaa-bb-cc-d";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("[bb] [cc] ", result);
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones guiones incompletos
        [Test]
        public void BuscarTest4()
        {
            //Arrange
            char caracter = '-';
            string cadena = "aaa-bbccc";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("Not found!", result);
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones de giones seguidos
        [Test]
        public void BuscarTest5()
        {
            //Arrange
            char caracter = '-';
            string cadena = "aaa-bb-cc--";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("[bb] [cc] ", result);
        }

        // Pruebas Unitarias de Utilidades.Buscar en condiciones de giones seguidos
        [Test]
        public void BuscarTest6()
        {
            //Arrange
            char caracter = '-';
            string cadena = "--";

            //Act
            string result = Utilidades.buscar(caracter, cadena);

            //Assert
            Assert.AreEqual("Not found!", result);
        }
    }
}